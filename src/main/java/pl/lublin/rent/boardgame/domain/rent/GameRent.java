package pl.lublin.rent.boardgame.domain.rent;

import lombok.Builder;
import lombok.Getter;
import pl.lublin.rent.boardgame.domain.boardgame.GameId;
import pl.lublin.rent.boardgame.domain.user.UserId;

import java.time.LocalDate;
import java.time.Period;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

@Getter
public class GameRent {

	private final GameRentId gameRentId;
	private final Set<GameId> gameIds;
	private final UserId userId;
	private final LocalDate rentDay;
	private final DayCount dayCount;
	private LocalDate returnDay;
	private RentState rentState;

	@Builder
	public GameRent(final GameRentId gameRentId, final Set<GameId> gameIds, final UserId userId, final RentState rentState,
			final LocalDate rentDay, final LocalDate returnDay, final DayCount dayCount) {
		this.gameRentId = requireNonNull(gameRentId, "GameRentId can't be null");
		this.gameIds = requireNonNull(gameIds, "GameId can't be null");
		checkArgument(gameIds.size() < 6, "Can't rent more than 5 games");
		this.userId = requireNonNull(userId, "UserName can't be null");
		this.rentState = requireNonNull(rentState, "RentState can't be null");
		this.rentDay = requireNonNull(rentDay, "RentDay can't be null");
		this.dayCount = requireNonNull(dayCount, "DayCount can't be null");
		this.returnDay = returnDay;
	}

	public Settlement returnToStock() {
		this.returnDay = LocalDate.now();
		final int rentPeriod = Period.between(rentDay, returnDay).getDays();
		if (rentPeriod > dayCount.getValue()) {
			this.rentState = RentState.EXPIRED;
			return new Settlement(Settlement.SettlementSide.USER, rentPeriod - dayCount.getValue());
		} else {
			this.rentState = RentState.RETURNED;
			return new Settlement(Settlement.SettlementSide.SYSTEM, dayCount.getValue() - rentPeriod);
		}
	}

	void rentGame() {
		this.rentState = RentState.RENT;
	}
}
