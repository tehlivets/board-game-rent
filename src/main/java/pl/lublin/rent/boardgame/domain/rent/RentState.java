package pl.lublin.rent.boardgame.domain.rent;

public enum RentState {
	DRAFT, RENT, RETURNED, EXPIRED
}
