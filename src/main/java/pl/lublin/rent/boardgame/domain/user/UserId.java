package pl.lublin.rent.boardgame.domain.user;

import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class UserId {

	private final String value;

	public UserId(final String value) {
		checkArgument(Strings.isEmpty(value), "Can't be empty");
		this.value = value;
	}
}
