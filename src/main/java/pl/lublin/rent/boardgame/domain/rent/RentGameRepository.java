package pl.lublin.rent.boardgame.domain.rent;

import org.springframework.stereotype.Repository;
import pl.lublin.rent.boardgame.domain.user.UserId;

@Repository
public interface RentGameRepository {
	boolean hasAnyUnfinishedRents(UserId userId);

	void save(GameRent gameRent);

	GameRent loadById(GameRent gameRent);
}
