package pl.lublin.rent.boardgame.domain.user;

public interface UserRepository {
	boolean isUserWithRebate(final UserId userId); // todo user with rents % 10 == 0
}
