package pl.lublin.rent.boardgame.domain.rent;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.lublin.rent.boardgame.domain.boardgame.BoardGame;
import pl.lublin.rent.boardgame.domain.user.UserId;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class GameRentFactory {

	public GameRent create(final String userId, final List<BoardGame> games, final Long dayCount) {

		return GameRent.builder()
				.dayCount(new DayCount(dayCount))
				.gameIds(games.stream().map(BoardGame::getGameId).collect(Collectors.toSet()))
				.gameRentId(new GameRentId(UUID.randomUUID().toString()))
				.rentDay(LocalDate.now())
				.rentState(RentState.DRAFT)
				.userId(new UserId(userId))
				.build();
	}
}
