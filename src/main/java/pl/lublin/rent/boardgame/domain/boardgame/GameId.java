package pl.lublin.rent.boardgame.domain.boardgame;

import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class GameId {

	private final String value;

	public GameId(final String value) {
		checkArgument(Strings.isEmpty(value), "Can't be empty");
		this.value = value;
	}
}
