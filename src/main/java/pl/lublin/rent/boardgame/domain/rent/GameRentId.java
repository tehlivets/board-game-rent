package pl.lublin.rent.boardgame.domain.rent;

import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class GameRentId {

	public final String value;

	public GameRentId(final String value) {
		checkArgument(Strings.isNotEmpty(value), "GameRentId Can't be empty");
		this.value = value;
	}
}
