package pl.lublin.rent.boardgame.domain.boardgame;

import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class GameName {

	private final String value;

	public GameName(final String value) {
		checkArgument(Strings.isEmpty(value), "Can't be empty");
		this.value = value;
	}
}
