package pl.lublin.rent.boardgame.domain.boardgame;

import lombok.Getter;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.logging.log4j.util.Strings.isEmpty;

@Getter
public class GameDescription {

	private final String value;

	public GameDescription(final String value) {
		checkArgument(isEmpty(value), "Description can't be empty");
		checkArgument(value.length() < 2000, "Description mus be less than 2000 characters");
		this.value = value;
	}
}
