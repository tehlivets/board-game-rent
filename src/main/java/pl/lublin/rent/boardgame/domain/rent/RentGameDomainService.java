package pl.lublin.rent.boardgame.domain.rent;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.lublin.rent.boardgame.domain.boardgame.Price;
import pl.lublin.rent.boardgame.domain.boardgame.PriceCurrency;
import pl.lublin.rent.boardgame.domain.user.UserId;

@RequiredArgsConstructor
@Service
public class RentGameDomainService implements RentDomainService {

	private final RentGameRepository rentGameRepository;
	private final RentPriceService rentPriceService;

	@Override
	public boolean isPossibleToRealizeRentByUser(final GameRent gameRent, final UserId userId) {
		return rentGameRepository.hasAnyUnfinishedRents(userId);
	}

	@Override
	public Price rent(final GameRent gameRent, final PriceCurrency priceCurrency) {
		gameRent.rentGame();
		rentGameRepository.save(gameRent);
		return rentPriceService.countPrice(gameRent, priceCurrency);
	}
}
