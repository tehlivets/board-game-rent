package pl.lublin.rent.boardgame.domain.rent;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.lublin.rent.boardgame.domain.boardgame.BoardGame;
import pl.lublin.rent.boardgame.domain.boardgame.DefaultRebatePolicy;
import pl.lublin.rent.boardgame.domain.boardgame.GameRebatePolicy;
import pl.lublin.rent.boardgame.domain.boardgame.Price;
import pl.lublin.rent.boardgame.domain.boardgame.PriceCurrency;
import pl.lublin.rent.boardgame.domain.boardgame.RebateFor10Rent;
import pl.lublin.rent.boardgame.domain.user.UserRepository;
import pl.lublin.rent.boardgame.infrastructure.boardgame.BoardGameRepository;

@Service
@RequiredArgsConstructor
public class ReturnRentPriceService {

	private final BoardGameRepository boardGameRepository;
	private final UserRepository userRepository;

	public Price countPrice(final GameRent gameRent, final PriceCurrency priceCurrency, Long days) {

		final GameRebatePolicy gameRebatePolicy = userRepository.isUserWithRebate(gameRent.getUserId()) ? new RebateFor10Rent() : new DefaultRebatePolicy();

		return boardGameRepository.findGamesByIds(gameRent.getGameIds())
				.stream()
				.peek(boardGame -> boardGame.setGameRebatePolicy(gameRebatePolicy))
				.map(boardGame -> calculateEndRentPrice(days, priceCurrency, boardGame))
				.reduce(Price::add)
				.orElseThrow(PriceCalculateException::new);
	}

	private Price calculateEndRentPrice(final Long dayCount, final PriceCurrency priceCurrency, final BoardGame boardGame) {
		return boardGame.getDayPrice(priceCurrency).multiply(dayCount).minus(boardGame.getGamePrice());
	}
}
