package pl.lublin.rent.boardgame.domain.user;

import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class UserName {

	private final String value;

	public UserName(final String value) {
		checkArgument(Strings.isEmpty(value), "Can't be empty");
		this.value = value;
	}
}
