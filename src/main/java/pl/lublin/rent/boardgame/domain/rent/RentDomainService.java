package pl.lublin.rent.boardgame.domain.rent;

import pl.lublin.rent.boardgame.domain.boardgame.Price;
import pl.lublin.rent.boardgame.domain.boardgame.PriceCurrency;
import pl.lublin.rent.boardgame.domain.user.UserId;

public interface RentDomainService {

	boolean isPossibleToRealizeRentByUser(GameRent gameRent, UserId userId);

	Price rent(GameRent gameRent, final PriceCurrency priceCurrency);
}
