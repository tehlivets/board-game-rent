package pl.lublin.rent.boardgame.application.boardgame.returngame;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.lublin.rent.boardgame.domain.boardgame.Price;
import pl.lublin.rent.boardgame.domain.boardgame.PriceCurrency;
import pl.lublin.rent.boardgame.domain.rent.GameRent;
import pl.lublin.rent.boardgame.domain.rent.GameRentId;
import pl.lublin.rent.boardgame.domain.rent.ReturnRentPriceService;
import pl.lublin.rent.boardgame.domain.rent.Settlement;
import pl.lublin.rent.boardgame.infrastructure.gamerent.GameRentRepository;

@Service
@RequiredArgsConstructor
public class ReturnBoardGameService {

	private final GameRentRepository gameRentRepository;

	private final ReturnRentPriceService returnRentPriceService;

	public Price returnBoardGameService(String gameRentId, PriceCurrency priceCurrency) {

		GameRent gameRent = gameRentRepository.loadGameRentId(new GameRentId(gameRentId));

		final Settlement settlement = gameRent.returnToStock();
		gameRentRepository.save(gameRent);

		final Price price = returnRentPriceService.countPrice(gameRent, priceCurrency, settlement.getDays());
		if (settlement.getPayTo() == Settlement.SettlementSide.USER) {
			return price;
		}
		return price.negate(priceCurrency);
	}
}
