package pl.lublin.rent.boardgame.application.boardgame.rent;

import lombok.RequiredArgsConstructor;
import pl.lublin.rent.boardgame.domain.boardgame.BoardGame;
import pl.lublin.rent.boardgame.domain.boardgame.Price;
import pl.lublin.rent.boardgame.domain.boardgame.PriceCurrency;
import pl.lublin.rent.boardgame.domain.rent.GameRent;
import pl.lublin.rent.boardgame.domain.rent.GameRentFactory;
import pl.lublin.rent.boardgame.domain.rent.RentDomainService;
import pl.lublin.rent.boardgame.infrastructure.boardgame.BoardGameRepository;

import javax.naming.OperationNotSupportedException;
import java.util.List;

@RequiredArgsConstructor
public class RentBoardGameService {

	private final BoardGameRepository boardGameRepository;

	private final RentDomainService rentDomainService;

	private final GameRentFactory gameRentFactory;

	public Price rent(String userId, Long dayCount, PriceCurrency priceCurrency, String... gameNames) throws OperationNotSupportedException {
		List<BoardGame> games = boardGameRepository.findGamesByNames(gameNames);

		GameRent gameRent = gameRentFactory.create(userId, games, dayCount);

		if (rentDomainService.isPossibleToRealizeRentByUser(gameRent, gameRent.getUserId())) {
			return rentDomainService.rent(gameRent, priceCurrency);
		} else {
			throw new OperationNotSupportedException("Solve all previous rents");
		}
	}
}
