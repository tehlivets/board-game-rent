package pl.lublin.rent.boardgame.infrastructure.gamerent;

import org.springframework.stereotype.Repository;
import pl.lublin.rent.boardgame.domain.rent.GameRent;
import pl.lublin.rent.boardgame.domain.rent.GameRentId;

@Repository
public interface GameRentRepository {
	GameRent loadGameRentId(GameRentId gameRentId);

	void save(GameRent gameRent);
}
