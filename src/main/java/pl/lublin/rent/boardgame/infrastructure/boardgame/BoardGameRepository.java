package pl.lublin.rent.boardgame.infrastructure.boardgame;

import org.springframework.stereotype.Repository;
import pl.lublin.rent.boardgame.domain.boardgame.BoardGame;
import pl.lublin.rent.boardgame.domain.boardgame.GameId;

import java.util.List;
import java.util.Set;

@Repository
public interface BoardGameRepository {
	List<BoardGame> findGamesByNames(String[] gameNames);

	List<BoardGame> findGamesByIds(Set<GameId> gameIds);
}
